import { Locator, Page } from '@playwright/test';

export default class ProductsPage {
    private readonly page: Page;

    public logo: Locator;

    public inventoryItems: Locator;

    constructor(page: Page) {
        this.page = page;
        this.logo = this.page.locator('.app_logo');
        this.inventoryItems = this.page.locator('.inventory_list .inventory_item');
    }

    public async isLogoDisplayed(): Promise<boolean> {
        await this.logo.waitFor({ state: 'visible', timeout: 15000 });

        return await this.logo.isVisible();
    }

    public async getLogoText() {
        return await this.logo.textContent();
    }

    public async getProductCardElement(productItem: number): Promise<Locator> {
        const inventoryItems = await this.inventoryItems.all();

        return inventoryItems[productItem];
    }

    public async getImgSource(productCardElement: Locator) {
        return await productCardElement.locator('.inventory_item_img img').getAttribute('src');
    }

    public async getProductName(productCardElement: Locator) {
        return await productCardElement.locator('.inventory_item_name').textContent();
    }

    public async getProductDescription(productCardElement: Locator) {
        return await productCardElement.locator('.inventory_item_desc').textContent();
    }

    public async getProductPrice(productCardElement: Locator) {
        return await productCardElement.locator('.inventory_item_price').textContent();
    }

    public async isAddCartBtnClickable(productCardElement: Locator) {
        return await productCardElement.locator('button.btn_primary').isEnabled();
    }

    public async getAddCartBtnText(productCardElement: Locator) {
        return await productCardElement.locator('button.btn_primary').textContent();
    }
}
