import { Locator, Page } from '@playwright/test';

export default class LoginPage {
    private readonly page: Page;

    public loginForm: Locator;

    public inputUsername: Locator;

    public inputPassword: Locator;

    public btnSubmit: Locator;

    public errorMessage: Locator;

    constructor(page: Page) {
        this.page = page;
        this.loginForm = this.page.locator('.login-box form');
        this.inputUsername = this.loginForm.locator('#user-name');
        this.inputPassword = this.loginForm.locator('#password');
        this.btnSubmit = this.loginForm.locator('#login-button');
        this.errorMessage = this.loginForm.locator('.error-message-container h3');
    }

    public visit() {
        return this.page.goto(`/`);
    }

    public async isOpened(): Promise<boolean> {
        await this.loginForm.waitFor({ state: 'visible', timeout: 5000 });

        return await this.loginForm.isVisible();
    }

    public async login(username: string, password: string): Promise<void> {
        await this.inputUsername.fill(username);
        await this.inputPassword.fill(password);
        await this.btnSubmit.click();
    }

    public async isErrorMessageVisible() {
        return await this.errorMessage.isVisible();
    }

    public async getErrorMessage() {
        return await this.errorMessage.textContent();
    }
}
