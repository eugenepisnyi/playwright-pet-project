import { test as base } from '@playwright/test';
import LoginPage from '../login.page';
import ProductsPage from '../products.page';

export const test = base.extend<{
    loginPage: LoginPage;
    productsPage: ProductsPage;
}>({
    // Define a fixtures
    loginPage: async ({ page }, use) => {
        await use(new LoginPage(page));
    },
    productsPage: async ({ page }, use) => {
        await use(new ProductsPage(page));
    },
});
