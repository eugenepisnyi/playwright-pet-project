## Getting started

### Installation

- The node version must be greater than or equal to version 20.11.1
  - You can download the latest version of node [here](https://nodejs.org/en/download)
- Install dependencies: `npm install`

## Configuration:

### Set environment variable:

Create .env file at the root of the directory with the structure:

```
LOCKED_USER_NAME=user_name
GLITCH_USER_NAME=user_name
STANDARD_USER_NAME=user_name

ROOT_USER_PASSWORD=user_password
```

You can get credentials of users from https://www.saucedemo.com/

## Running tests

### Running the Example Test in UI Mode

Run your tests with UI Mode for a better developer experience with time travel debugging:

- `npx playwright test --ui`

### CLI options

| Parameter |  Type   | Description                                                           |
| :-------- | :-----: | :-------------------------------------------------------------------- |
| --help    | Boolean | Ask for help.                                                         |
| --workers | Number  | Run tests in in parallel. Default = `1`.                              |
| -g        | String  | Run the test with the title.                                          |
| --headed  | Boolean | Run tests in headed browsers. Default = `true`.                       |
| --project | String  | Run all the tests against a specific project. `[chromium \ firefox ]` |
| --debug   | Boolean | Run tests in debug mode with Playwright Inspector.                    |

### Spec execution

### Spec execution

- Run **`npm test -- -g "test title"`**
- Run **`npm test -- --project=chromium --headed specs/yourtest_1.spec.tss`**
- Run **`npm test -- --project=chromium --headed specs/yourtest_1.spec.ts specs/yourtest_2.spec.ts`**
