import { defineConfig, devices } from '@playwright/test';

export default defineConfig({
    testDir: './specs',
    testMatch: 'specs/**/*.spec.ts',
    timeout: 60 * 1000,
    expect: {
        timeout: 15000,
    },
    fullyParallel: true,
    forbidOnly: !!process.env.CI,
    retries: process.env.CI ? 2 : 0,
    workers: process.env.CI ? 1 : undefined,
    reporter: [['list', { printSteps: true }], ['html']],
    outputDir: 'test-results-artifacts',
    use: {
        baseURL: 'https://www.saucedemo.com/',
        trace: 'retain-on-failure',
        screenshot: 'only-on-failure',
        ignoreHTTPSErrors: true,
        headless: true,
        viewport: { width: 1920, height: 1080 },
        launchOptions: {
            slowMo: 350,
        },
    },
    projects: [
        {
            name: 'chromium',
            use: { ...devices['Desktop Chrome'] },
        },
        {
            name: 'firefox',
            use: { ...devices['Desktop Firefox'] },
        },
    ],
});
