import * as dotenv from 'dotenv';
import { UsersCredentialInterfaces } from './interfaces/users.credential.interfaces';
dotenv.config();

const not_indicated: string = 'not indicated';

const locked_user: UsersCredentialInterfaces = {
    name: process.env.LOCKED_USER_NAME || not_indicated,
    password: process.env.ROOT_USER_PASSWORD || not_indicated,
};

const glitch_user: UsersCredentialInterfaces = {
    name: process.env.GLITCH_USER_NAME || not_indicated,
    password: process.env.ROOT_USER_PASSWORD || not_indicated,
};

const standard_user: UsersCredentialInterfaces = {
    name: process.env.STANDARD_USER_NAME || not_indicated,
    password: process.env.ROOT_USER_PASSWORD || not_indicated,
};

export { locked_user, glitch_user, standard_user };
