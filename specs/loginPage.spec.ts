import { expect } from '@playwright/test';
import { glitch_user, locked_user, standard_user } from '../data/users.credentials.data';
import errorsMessagesData from '../data/errors.messages.data';
import headerData from '../data/header.data';
import { test } from '../pages/fixtures/pages.fixtures';

test.describe('Login page', () => {
    test.beforeEach(async ({ loginPage }) => {
        await loginPage.visit();
        expect(await loginPage.isOpened()).toBe(true);
    });

    test('should not login with locked user', async ({ loginPage }) => {
        await loginPage.login(locked_user.name, locked_user.password);
        expect(await loginPage.getErrorMessage()).toBe(errorsMessagesData.locked_user);
    });

    test('should login with performance glitch user', async ({ loginPage, productsPage }) => {
        await test.step('no error message on login page', async () => {
            await loginPage.login(glitch_user.name, glitch_user.password);
            expect(await loginPage.isErrorMessageVisible()).toBe(false);
        });

        await test.step('should displayed the Product page', async () => {
            expect(await productsPage.isLogoDisplayed()).toBe(true);
            expect(await productsPage.getLogoText()).toBe(headerData.logo_text);
        });
    });

    test('should login with standard user', async ({ loginPage, productsPage }) => {
        await test.step('should login with standard user', async () => {
            await loginPage.login(standard_user.name, standard_user.password);
            expect(await loginPage.isErrorMessageVisible()).toBe(false);
        });

        await test.step('should displayed the Product page', async () => {
            expect(await productsPage.isLogoDisplayed()).toBe(true);
            expect(await productsPage.getLogoText()).toBe(headerData.logo_text);
        });
    });
});
