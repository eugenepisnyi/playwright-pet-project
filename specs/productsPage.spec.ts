import { standard_user } from '../data/users.credentials.data.js';
import { productsCardData } from '../data/products.data.js';
import { test } from '../pages/fixtures/pages.fixtures';
import { expect } from '@playwright/test';

test.describe('Product page', () => {
    test.beforeEach(async ({ loginPage, productsPage }) => {
        await loginPage.visit();
        expect(await loginPage.isOpened()).toBe(true);
        await loginPage.login(standard_user.name, standard_user.password);
        expect(await productsPage.isLogoDisplayed()).toBe(true);
    });

    Object.keys(productsCardData).forEach((key, index) => {
        test(`should contains correct ${productsCardData[key].name} product's card`, async ({ productsPage }) => {
            const productCardElement = await productsPage.getProductCardElement(index);
            expect(await productsPage.getImgSource(productCardElement)).toBe(productsCardData[key].imgSource);
            expect(await productsPage.getProductName(productCardElement)).toBe(productsCardData[key].name);
            expect(await productsPage.getProductDescription(productCardElement)).toBe(productsCardData[key].description);
            expect(await productsPage.getProductPrice(productCardElement)).toBe(productsCardData[key].price);
            expect(await productsPage.isAddCartBtnClickable(productCardElement)).toBe(true);
            expect(await productsPage.getAddCartBtnText(productCardElement)).toBe('Add to cart');
        });
    });
});
