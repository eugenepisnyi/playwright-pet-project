'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
exports.standard_user = exports.glitch_user = exports.locked_user = void 0;
const dotenv = require('dotenv');
dotenv.config();
const not_indicated = 'not indicated';
const locked_user = {
    name: process.env.LOCKED_USER_NAME || not_indicated,
    password: process.env.ROOT_USER_PASSWORD || not_indicated,
};
exports.locked_user = locked_user;
const glitch_user = {
    name: process.env.GLITCH_USER_NAME || not_indicated,
    password: process.env.ROOT_USER_PASSWORD || not_indicated,
};
exports.glitch_user = glitch_user;
const standard_user = {
    name: process.env.STANDARD_USER_NAME || not_indicated,
    password: process.env.ROOT_USER_PASSWORD || not_indicated,
};
exports.standard_user = standard_user;
//# sourceMappingURL=users.credentials.data.js.map
