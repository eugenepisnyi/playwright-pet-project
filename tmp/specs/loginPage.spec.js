"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const test_1 = require("@playwright/test");
const users_credentials_data_1 = require("../data/users.credentials.data");
const errors_messages_data_1 = require("../data/errors.messages.data");
const header_data_1 = require("../data/header.data");
const pages_fixtures_1 = require("../pages/fixtures/pages.fixtures");
pages_fixtures_1.test.describe('Login page', () => {
    pages_fixtures_1.test.beforeEach(async ({ loginPage }) => {
        await loginPage.visit();
        (0, test_1.expect)(await loginPage.isOpened()).toBe(true);
    });
    (0, pages_fixtures_1.test)('should not login with locked user', async ({ loginPage }) => {
        await loginPage.login(users_credentials_data_1.locked_user.name, users_credentials_data_1.locked_user.password);
        (0, test_1.expect)(await loginPage.getErrorMessage()).toBe(errors_messages_data_1.default.locked_user);
    });
    (0, pages_fixtures_1.test)('should login with performance glitch user', async ({ loginPage, productsPage }) => {
        await pages_fixtures_1.test.step('no error message on login page', async () => {
            await loginPage.login(users_credentials_data_1.glitch_user.name, users_credentials_data_1.glitch_user.password);
            (0, test_1.expect)(await loginPage.isErrorMessageVisible()).toBe(false);
        });
        await pages_fixtures_1.test.step('should displayed the Product page', async () => {
            (0, test_1.expect)(await productsPage.isLogoDisplayed()).toBe(true);
            (0, test_1.expect)(await productsPage.getLogoText()).toBe(header_data_1.default.logo_text);
        });
    });
    (0, pages_fixtures_1.test)('should login with standard user', async ({ loginPage, productsPage }) => {
        await pages_fixtures_1.test.step('should login with standard user', async () => {
            await loginPage.login(users_credentials_data_1.standard_user.name, users_credentials_data_1.standard_user.password);
            (0, test_1.expect)(await loginPage.isErrorMessageVisible()).toBe(false);
        });
        await pages_fixtures_1.test.step('should displayed the Product page', async () => {
            (0, test_1.expect)(await productsPage.isLogoDisplayed()).toBe(true);
            (0, test_1.expect)(await productsPage.getLogoText()).toBe(header_data_1.default.logo_text);
        });
    });
});
//# sourceMappingURL=loginPage.spec.js.map