"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const users_credentials_data_js_1 = require("../data/users.credentials.data.js");
const products_data_js_1 = require("../data/products.data.js");
const pages_fixtures_1 = require("../pages/fixtures/pages.fixtures");
const test_1 = require("@playwright/test");
pages_fixtures_1.test.describe('Product page', () => {
    pages_fixtures_1.test.beforeEach(async ({ loginPage, productsPage }) => {
        await loginPage.visit();
        (0, test_1.expect)(await loginPage.isOpened()).toBe(true);
        await loginPage.login(users_credentials_data_js_1.standard_user.name, users_credentials_data_js_1.standard_user.password);
        (0, test_1.expect)(await productsPage.isLogoDisplayed()).toBe(true);
    });
    Object.keys(products_data_js_1.productsCardData).forEach((key, index) => {
        (0, pages_fixtures_1.test)(`should contains correct ${products_data_js_1.productsCardData[key].name} product's card`, async ({ productsPage }) => {
            const productCardElement = await productsPage.getProductCardElement(index);
            (0, test_1.expect)(await productsPage.getImgSource(productCardElement)).toBe(products_data_js_1.productsCardData[key].imgSource);
            (0, test_1.expect)(await productsPage.getProductName(productCardElement)).toBe(products_data_js_1.productsCardData[key].name);
            (0, test_1.expect)(await productsPage.getProductDescription(productCardElement)).toBe(products_data_js_1.productsCardData[key].description);
            (0, test_1.expect)(await productsPage.getProductPrice(productCardElement)).toBe(products_data_js_1.productsCardData[key].price);
            (0, test_1.expect)(await productsPage.isAddCartBtnClickable(productCardElement)).toBe(true);
            (0, test_1.expect)(await productsPage.getAddCartBtnText(productCardElement)).toBe('Add to cart');
        });
    });
});
//# sourceMappingURL=productsPage.spec.js.map