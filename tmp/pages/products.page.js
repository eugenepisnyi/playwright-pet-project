'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
class ProductsPage {
    constructor(page) {
        this.page = page;
        this.logo = this.page.locator('.app_logo');
        this.inventoryItems = this.page.locator('.inventory_list .inventory_item');
    }
    async isLogoDisplayed() {
        await this.logo.waitFor({ state: 'visible', timeout: 15000 });
        return await this.logo.isVisible();
    }
    async getLogoText() {
        return await this.logo.textContent();
    }
    async getProductCardElement(productItem) {
        const inventoryItems = await this.inventoryItems.all();
        return inventoryItems[productItem];
    }
    async getImgSource(productCardElement) {
        return await productCardElement.locator('.inventory_item_img img').getAttribute('src');
    }
    async getProductName(productCardElement) {
        return await productCardElement.locator('.inventory_item_name').textContent();
    }
    async getProductDescription(productCardElement) {
        return await productCardElement.locator('.inventory_item_desc').textContent();
    }
    async getProductPrice(productCardElement) {
        return await productCardElement.locator('.inventory_item_price').textContent();
    }
    async isAddCartBtnClickable(productCardElement) {
        return await productCardElement.locator('button.btn_primary').isEnabled();
    }
    async getAddCartBtnText(productCardElement) {
        return await productCardElement.locator('button.btn_primary').textContent();
    }
}
exports.default = ProductsPage;
//# sourceMappingURL=products.page.js.map
