'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
exports.test = void 0;
const test_1 = require('@playwright/test');
const login_page_1 = require('../login.page');
const products_page_1 = require('../products.page');
exports.test = test_1.test.extend({
    // Define a fixtures
    loginPage: async ({ page }, use) => {
        await use(new login_page_1.default(page));
    },
    productsPage: async ({ page }, use) => {
        await use(new products_page_1.default(page));
    },
});
//# sourceMappingURL=pages.fixtures.js.map
