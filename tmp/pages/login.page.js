'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
class LoginPage {
    constructor(page) {
        this.page = page;
        this.loginForm = this.page.locator('.login-box form');
        this.inputUsername = this.loginForm.locator('#user-name');
        this.inputPassword = this.loginForm.locator('#password');
        this.btnSubmit = this.loginForm.locator('#login-button');
        this.errorMessage = this.loginForm.locator('.error-message-container h3');
    }
    visit() {
        return this.page.goto(`/`);
    }
    async isOpened() {
        await this.loginForm.waitFor({ state: 'visible', timeout: 5000 });
        return await this.loginForm.isVisible();
    }
    async login(username, password) {
        await this.inputUsername.fill(username);
        await this.inputPassword.fill(password);
        await this.btnSubmit.click();
    }
    async isErrorMessageVisible() {
        return await this.errorMessage.isVisible();
    }
    async getErrorMessage() {
        return await this.errorMessage.textContent();
    }
}
exports.default = LoginPage;
//# sourceMappingURL=login.page.js.map
